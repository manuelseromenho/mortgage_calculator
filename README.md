### Mortgage Calculator

Using Django 2.2.4 and Python 3.9

Calculate the monthly payments of a fixed term mortgage over given Nth terms at a given interest 
rate. Also figure out how long it will take the user to pay back the loan. For added complexity, add an option for users 
to select the compounding interval (Monthly, Weekly, Daily, Continually).

https://repl.it/@manuelseromenho/Mortgage-Calculator-Django